/* 
	Problem 5: Write a function that will use the previously written functions
    to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

const lists = require('./data/lists_1.json')
const boards = require('./data/boards_1.json')
const cards = require('./data/cards_1.json')

const getBoardInformation = require('./callback1.cjs')
const listsForBoard = require('./callback2.cjs')
const cardInfoWithListId = require('./callback3.cjs')

const boardID = 'mcu453ed'

function mindAndSpace () {
  getBoardInformation(boards, boardID)
    .then(board => {
      console.log(board)
      const boardId = board.id
      console.log('board : ', boardId)
      return listsForBoard(boardId, lists)
    })
    .then(listOfBoard => {
      console.log('list board : ', listOfBoard)
      const listMind = listOfBoard.filter(element => element.name === 'Mind')
      const listSpace = listOfBoard.filter(element => element.name === 'Space')
      list = [listMind[0].id, listSpace[0].id]
      return list
    })
    .then(list => {
      list.map(element => {
        cardInfoWithListId(element, cards)
          .then(result => {
            console.log(`\n ${element} :`, result)
          })
          .catch(error => {
            console.log(error)
          })
      })
    })
    .catch(error => {
      console.log(error)
    })
}

module.exports = mindAndSpace

module.exports = mindAndSpace