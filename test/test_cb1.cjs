const getBoardInformation = require('../callback1.cjs')
const boards = require('../data/boards_1.json')
const boardID = 'mcu453ed'

getBoardInformation(boards, boardID)
  .then(board => {
    console.log(board)
    return
  })
  .catch(error => {
    console.log(error)
  })
