const lists = require('../data/lists_1.json')
const boards = require('../data/boards_1.json')
const cards = require('../data/cards_1.json')

const cardInfoWithListId = require('../callback3.cjs')

const listId = 'qwsa221'
cardInfoWithListId(listId, cards)
  .then(result => {
    console.log(result)
  })
  .catch(err => {
    console.log(err)
  })
