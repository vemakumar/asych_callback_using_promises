/* 
	Problem 6: Write a function that will use the previously written functions 
    to get the following information. You do not need to pass control back to the
    code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

const lists = require('./data/lists_1.json')
const boards = require('./data/boards_1.json')
const cards = require('./data/cards_1.json')

const getBoardInformation = require('./callback1.cjs')
const listsForBoard = require('./callback2.cjs')
const cardInfoWithListId = require('./callback3.cjs')

const boardID = 'mcu453ed'

function allCards () {
  getBoardInformation(boards, boardID)
    .then(board => {
      console.log(board)
      const boardId = board.id
      console.log('board : ', boardId)
      return listsForBoard(boardId, lists)
    })
    .then(listOfBoard => {
      console.log('list board : ', listOfBoard)
      const list = listOfBoard.map(element => element.id)
      console.log(list)
      return list
    })
    .then(list => {
      list.map(element => {
        cardInfoWithListId(element, cards)
          .then(result => {
            console.log(`\n ${element} :`, result)
          })
          .catch(error => {
            console.log(error)
          })
      })
    })
    .catch(error => {
      console.log(error)
    })
}

module.exports = allCards
